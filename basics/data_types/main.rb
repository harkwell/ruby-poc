#!/usr/bin/env ruby

# ruby main.rb

###############
### CLASSES ###
###############
class Clazz
	def initialize(name = "foo")
		@name = name
	end
	def method1
		puts "Name is #{@name}."
	end
end

#############
### ENUMS ###
#############
module Animals
	MONKEY = 0
	GIRAFFE = 1
	ELEPHANT = 2
	DOG = 3
	CAT = 4
end

def primitives
	int = 1024
	puts int
	float = 3.14159265359
	puts float
	boolean = (5 == 5)
	puts boolean
	string = "foo"
	puts string
	hash = { "key1" => "val1", "key2" => "val2" }
	t = int
	t = float
	t = boolean
	t = string
	t = hash
	print t
	puts
	puts int.class
	puts float.class
	puts boolean.class
	puts string.class
	puts hash.class
end

def arrays
	array = [ 1, 2.0, true, "foo", { "foo" => "bar" }, [ 3, 4] ]
	puts array.class
	print array
	puts
	array.insert(1, 1.5)
	array.delete_at(0)
	array.pop
	array << "end-of-array"
	print array
	puts
end

def keywords
	string = "__ENCODING__ __LINE__ __FILE__ BEGIN END alias and begin " \
		+"break case class def defined?  do else elsif end ensure " \
		+"false for if in module next nil not or redo rescue retry " \
		+"return self super then true undef unless until when while " \
		+"yield"

	for keyword in string.split(" ") do
		print "#{keyword} "
	end
	puts
end

def enums
	for enum in Animals.constants do
		if enum == Animals::CAT
			print "CHOSEN -> #{enum} "
		else
			print "#{enum} "
		end
	end
	puts
end

def operators
	puts "The + operator (addition)."
	int = 1 + 1
	float = 1.5 + 0.5
	float = 2 + 0.5
	string = "1" + "1"
	puts "The - operator (subtraction)."
	float = 2 - 0.5
	puts "The * operator (multiplication)."
	int = 2 * 2
	puts "The ** operator (power)."
	int = 2 ** 4
	puts "The / operator (division)."
	int = 4 / 2
	int = 3 / 2
	float = 3.0 / 2
	puts "The % operator (modulo)."
	int = 3 % 2
	puts "The == operator (comparison)."
	boolean = (10 == 10)
	puts "The to_s(2) operator (bitwise toString())."
	string = 1023.to_s(2)
	puts "The & operator (bitwise AND)."
	int = 7 & 4
	puts "The | operator (bitwise OR)."
	int = 0 | 1
	puts "The ^ operator (bitwise XOR)."
	int = 5 ^ 3
	puts "The ~ operator (bitwise NOT)."
	int = ~5
	puts "The << operator (bitwise shift left)."
	int = 2 << 1
	puts "The >> operator (bitwise shift right)."
	int = 2 >> 1
	puts "The [] and []= operators (index)."
end

def precedence
	puts "Order of Precedence (many can be overridden!):"
	puts "[], []="
	puts "**"
	puts "!, ~, +, -"
	puts "*, /, %"
	puts "+, -"
	puts "<<, >>"
	puts "&"
	puts "^, |"
	puts "<=, <, >, >="
	puts "<=>, ==, ===, !=, =~, !~"
	puts "&&"
	puts "||"
	puts ".., ..."
	puts "? :"
	puts "=, %=, {, /=, -=, +=, |=, &=, >>=, <<=, *=, &&="
	puts "||=, **="
	puts "defined?"
	puts "not"
	puts "or, and"
	puts "if, unless, while, until"
	puts "begin, end"
end

############
### MAIN ### main
############
### keywords
keywords

### primitives
primitives

### operators
operators

### precedence
precedence

### classes
foo = Clazz.new("khallware")
#puts Clazz.instance_methods
puts Clazz.instance_methods(false)
foo.method1

if (foo.is_a? Clazz)
	puts "foo is a Clazz"
end

### arrays
#puts Array.instance_methods(false)
arrays

### enums
enums
