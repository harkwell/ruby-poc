Ruby on Rails (Hello World)
=================

Installation
---------------
```shell
chromium-browser https://guides.rubyonrails.org/getting_started.html
docker run -it -h rails --name rails -v /tmp/rails:/tmp centos
# we want ruby 2.5.1 (28 March 2018)
yum install -y gcc-c++ patch readline readline-devel zlib zlib-devel \
   libyaml-devel libffi-devel openssl-devel make bzip2 autoconf automake \
   libtool bison iconv-devel sqlite-devel
yum install -y git which
curl -sSL https://rvm.io/mpapis.asc | gpg --import -
curl -L get.rvm.io | bash -s stable
source /etc/profile.d/rvm.sh
rvm reload
rvm requirements run
rvm install 2.5

gem update --system
gem install rails
cd /tmp && git clone https://gitlab.com/harkwell/ruby-poc.git && cd ruby-poc/rails
rails new hello_world && cd hello_world
rm -rf .git/
sed -i -e "s/^# gem 'mini_racer'/gem 'mini_racer'/" Gemfile
bundle install
#rails generate controller pages
git checkout -f
./bin/rails server

# external to docker
IPADDR=$(docker inspect rails |grep IPAddress |head -1 |cut -d\" -f4)
chromium-browser http://$IPADDR:3000/
```
