Khallware (Ruby Proofs-of-Concept)
=================
Overview
---------------
Here are some simple proofs-of-concept written in ruby.

Installation
---------------
```shell
docker run -it -h ruby --name ruby centos
yum install -y ruby
echo 'puts "Hello World"' |irb

yum install -y git
git clone https://gitlab.com/harkwell/ruby-poc.git && cd ruby-poc
```
