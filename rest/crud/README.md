CRUD RESTful Web Service
=================
Overview
---------------
A simple proof-of-concept in writing a RESTful webservice in the ruby
programming language.  It shows how to persist entities in a sqlite
database for a simple microservice (food recipes).

Quick Start
---------------
```shell
mkdir -p /tmp/recipes
docker docker run -it -h recipes --name recipes -v /tmp/recipes:/tmp centos bash
yum install -y ruby sqlite-devel ruby-devel make gcc
gem install sinatra sqlite3 json
sqlite3 recipes.db <db_schema.sql
ruby main.rb

# from another shell
IPADDR=$(docker inspect recipes |grep IPAddress |cut -d\" -f4 |head)
curl -i -X POST -d '{ "name" : "test1", "items" : [ "1cup:milk", "2:eggs" ], "steps" : [ "preheat to 400 degrees", "mix milk and eggs", "bake for twenty minutes" ], "outputs" : "4 servings" }' http://$IPADDR:4567/recipes
curl -i -X POST -d '{ "name" : "test2", "items" : [ "1cup:sugar", "2tsp:vanilla" ], "steps" : [ "preheat to 450 degrees", "mix ingredients", "bake for thirty minutes" ], "outputs" : "5 servings" }' http://$IPADDR:4567/recipes
curl -i -X GET http://$IPADDR:4567/recipes
curl -i -X GET http://$IPADDR:4567/recipes?name=test1
curl -i -X PUT -d '{ "name" : "test1", "items" : [ "1cup:milk", "4:eggs" ], "steps" : [ "preheat to 400 degrees", "mix milk and eggs", "bake for twenty minutes" ], "outputs" : "4 servings" }' http://$IPADDR:4567/recipes
curl -i -X GET http://$IPADDR:4567/recipes?name=test1
curl -i -X DELETE http://$IPADDR:4567/recipes?name=test1
curl -i -X DELETE http://$IPADDR:4567/recipes?name=test2
```
