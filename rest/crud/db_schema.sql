-- ============================================================================
-- This script creates the structure of the recipe database.
--
-- sqlite3 recipes.db <db_schema.sql
-- ============================================================================
CREATE TABLE recipes (
        name varchar(1024),
        items varchar(1024),
        steps varchar(1024),
        output varchar(1024)
);

