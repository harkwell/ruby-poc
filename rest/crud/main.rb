#!/usr/bin/env ruby

require 'sinatra'   # http://www.sinatrarb.com/configuration.html
require 'json'      # http://flori.github.io/json/
require './recipe'
require './datastore'

set :bind, '0.0.0.0'

##############
### CREATE ###
##############
post '/recipes' do
	content_type :json
	retval = Recipe.from_json(request.body.read)
	Datastore.save(retval)
	retval.to_json
end

############
### READ ###
############
get '/recipes' do
	content_type :json

	retval = (params[:name]) \
		? Datastore.list(params[:name]) \
		: Datastore.list_all()
	retval.to_json
end

##############
### UPDATE ###
##############
put '/recipes' do
	content_type :json
	retval = Recipe.from_json(request.body.read)

	if Datastore.count(retval.name) > 0
		Datastore.delete(retval.name)
		Datastore.save(retval)
	else
		retval = RecipeBuilder.builder().build()
	end
	retval.to_json
end

##############
### DELETE ###
##############
delete '/recipes' do
	content_type :json
	retval = Datastore.list(params[:name])
	Datastore.delete(params[:name])
	retval.to_json
end
