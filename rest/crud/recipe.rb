### This is the Recipe class

require 'logger'

class RecipeBuilder
	def initialize()
		@retval = Recipe.new
	end

	def self.builder()
		RecipeBuilder.new
	end

	def name(name)
		@retval.name = name
		@self
	end

	def item(item)
		@retval.items.insert(-1, item)
		@self
	end

	def step(step)
		@retval.steps.insert(-1, step)
		@self
	end

	def output(output)
		@retval.name = output
		@self
	end

	def build()
		@retval
	end
end

class Recipe
	attr_accessor :name, :items, :steps, :output

	def initialize()    #(&block)
		@items = []
		@steps = []
		#instance_eval &block if block_given?
	end

	########################
	### from_json method ###
	########################
	def self.from_json(content)
		retval = RecipeBuilder.builder()
		logger = Logger.new($stdout)
		logger.debug("content: #{content}")
		json = JSON.parse(content)
		retval.name(json["name"]) if json.key? "name"
		retval.output(json["output"]) if json.key? "output"

		json["items"].each do |item|
			retval.item(item)
		end
		json["steps"].each do |step|
			retval.step(step)
		end
		retval.build()
	end

	########################
	### to string method ###
	########################
	def to_s
		"name: #{name}"+" items: #{items.join(",")}" \
		+" steps: #{steps.join(",")}"+" outputs: #{output}"
	end
end
