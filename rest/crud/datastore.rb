### This is the Datastore class

require 'sqlite3'   # http://sqlite-ruby.rubyforge.org/sqlite3/faq.html
require 'logger'
require './recipe'

class Datastore
	def self.save(recipe)
		begin
			logger = Logger.new($stdout)
			db = SQLite3::Database.open "recipes.db"
			sql = <<EOF.gsub(/\s+/, " ").strip
				INSERT INTO recipes
				VALUES('#{recipe.name}',
					'#{recipe.items}',
					'#{recipe.steps}',
					'#{recipe.output}')
EOF
			logger.debug("SQL: #{sql}")
			db.execute(sql)
		rescue SQLite3::Exception => e
			puts "ERROR: "+e
		ensure
			db.close if db
		end
	end

	def self.list_all()
		retval = []
		begin
			logger = Logger.new($stdout)
			db = SQLite3::Database.open "recipes.db"
			sql = "SELECT * FROM recipes"
			logger.debug("SQL: #{sql}")
			retval = db.execute(sql)
		rescue SQLite3::Exception => e
			puts "ERROR: "+e
		ensure
			db.close if db
		end
		retval
	end

	def self.list(name)
		retval = []
		begin
			logger = Logger.new($stdout)
			db = SQLite3::Database.open "recipes.db"
			sql = "SELECT * FROM recipes WHERE name = '#{name}'"
			logger.debug("SQL: #{sql}")
			retval = db.execute(sql)
		rescue SQLite3::Exception => e
			puts "ERROR: "+e
		ensure
			db.close if db
		end
		retval
	end

	def self.delete(name)
		begin
			logger = Logger.new($stdout)
			db = SQLite3::Database.open "recipes.db"
			sql = "DELETE FROM recipes WHERE name = '#{name}'"
			logger.debug("SQL: #{sql}")
			db.execute(sql)
		rescue SQLite3::Exception => e
			puts "ERROR: "+e
		ensure
			db.close if db
		end
	end

	def self.count(name)
		retval = 0
		sql = "SELECT COUNT(*) FROM recipes WHERE name = '#{name}'"
		begin
			logger = Logger.new($stdout)
			db = SQLite3::Database.open "recipes.db"
			logger.debug("SQL: #{sql}")
			retval = db.get_first_value(sql)
		rescue SQLite3::Exception => e
			puts "ERROR: "+e
		ensure
			db.close if db
		end
		retval
	end
end
