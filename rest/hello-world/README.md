Hello World RESTful Web Service
=================
Overview
---------------
A simple proof-of-concept in writing a RESTful webservice in the ruby
programming language.

Quick Start
---------------
```shell
docker docker run -it -h ruby --name ruby centos bash
yum install -y ruby
gem install sinatra
ruby main.rb

# from another shell
IPADDR=$(docker inspect ruby |grep IPAddress |cut -d\" -f4 |head)
curl -i -X GET http://$IPADDR:4567/greetings/
```
