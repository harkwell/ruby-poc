#!/usr/bin/env ruby

require 'sinatra'   # http://www.sinatrarb.com/configuration.html
require './greeter'

set :bind, '0.0.0.0'

get '/greetings' do
	greeter = Greeter.new
	greeter.interact
end
